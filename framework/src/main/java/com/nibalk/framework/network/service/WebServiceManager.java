package com.nibalk.framework.network.service;


import io.reactivex.Observable;
import retrofit2.Call;

public interface WebServiceManager {

    /**
     * For making network calls from the app
     * @param call Retrofit call
     * @return Network response as an Observable
     */
    <T> Observable<T> execute(Call<T> call);
}
