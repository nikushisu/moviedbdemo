package com.nibalk.moviedb.app.di.module;

import android.content.Context;

import com.nibalk.moviedb.app.data.repository.MovieRepository;
import com.nibalk.moviedb.app.data.repository.MovieRepositoryImpl;
import com.nibalk.moviedb.app.data.service.MovieDbService;
import com.nibalk.framework.network.service.WebServiceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class MovieDbModule {

    @Provides
    @Singleton
    MovieRepository provideMovieRepository(Context context,
                                               MovieDbService movieDbService,
                                               WebServiceManager webServiceManager) {
        return new MovieRepositoryImpl(context, movieDbService, webServiceManager);
    }

    @Provides
    @Singleton
    MovieDbService provideMovieDbService(Retrofit retrofit) {
        return retrofit.create(MovieDbService.class);
    }
}
