package com.nibalk.moviedb.app.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.nibalk.moviedb.app.data.model.Movie;

import java.util.List;

import static com.nibalk.moviedb.app.utils.Constants.Values.DB_TABLE_NAME;

@Dao
public interface MoviesDao {
    @Query("SELECT * FROM " + DB_TABLE_NAME)
    List<Movie> selectAllMovies();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMovies(Movie movie);

    @Query("DELETE FROM " + DB_TABLE_NAME)
    abstract void deleteAllMovies();
}
