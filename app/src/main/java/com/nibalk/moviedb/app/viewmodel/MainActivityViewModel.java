package com.nibalk.moviedb.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.repository.MovieRepository;
import com.nibalk.framework.base.viewmodel.BaseViewModel;

public class MainActivityViewModel extends BaseViewModel {

    private MovieRepository repository;

    public MainActivityViewModel(MovieRepository repository) {
        this.repository = repository;
    }

    public LiveData<PagedList<Movie>> getNowPlayingMovies() {
        repository.initFetching(getCompositeDisposable(), null);
        return repository.getMovies();
    }

    public LiveData<ProgressStatus> getProgressStatus() {
        return repository.getProgressStatus();
    }
}
