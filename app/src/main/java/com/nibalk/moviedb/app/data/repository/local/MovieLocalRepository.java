package com.nibalk.moviedb.app.data.repository.local;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.nibalk.moviedb.app.data.datasource.local.MovieLocalDataSourceFactory;
import com.nibalk.moviedb.app.data.model.Movie;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.nibalk.moviedb.app.utils.Constants.Values.NUMBERS_OF_THREADS;

public class MovieLocalRepository {

    final private static String TAG = "DUApp-" + MovieLocalRepository.class.getSimpleName();

    private LiveData<PagedList<Movie>> moviesPagedList;

    public MovieLocalRepository(MovieLocalDataSourceFactory dataSourceFactory) {
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(Integer.MAX_VALUE)
                .setPageSize(Integer.MAX_VALUE).build();

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder(
                dataSourceFactory, pagedListConfig);
        moviesPagedList = livePagedListBuilder.setFetchExecutor(executor).build();
    }

    public LiveData<PagedList<Movie>> getMoviesPagedList() {
        return moviesPagedList;
    }
}
