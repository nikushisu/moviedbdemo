package com.nibalk.moviedb.app.data.datasource.remote;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.service.MovieDbService;
import com.nibalk.framework.network.service.WebServiceManager;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.ReplaySubject;


public class MovieRemoteDataSourceFactory extends DataSource.Factory {


    private MutableLiveData<MovieRemotePageKeyedDataSource> remoteDataSourceLive;
    private MovieRemotePageKeyedDataSource remoteDataSource;

    public MovieRemoteDataSourceFactory(MovieDbService movieDbService,
                                        WebServiceManager webServiceManager,
                                        CompositeDisposable compositeDisposable,
                                        String selectedMovieId) {
        this.remoteDataSource = new MovieRemotePageKeyedDataSource(
                movieDbService, webServiceManager, compositeDisposable, selectedMovieId);
        this.remoteDataSourceLive = new MutableLiveData<>();
    }

    @Override
    public DataSource create() {
        remoteDataSourceLive.postValue(remoteDataSource);
        return remoteDataSource;
    }

    public MutableLiveData<MovieRemotePageKeyedDataSource> getRemoteDataSourceLive() {
        return remoteDataSourceLive;
    }

    public ReplaySubject<Movie> getMovies() {
        return remoteDataSource.getMovies();
    }
}
