package com.nibalk.moviedb.app.utils;

public class Constants {

    public static class AppUrls {
        public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
        public static final String FETCH_NOW_PLAYING = "now_playing";
        public static final String FETCH_SIMILAR_MOVIES = "{movie_id}/similar";
        public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/";
        public static final String FETCH_W500_IMAGE = BASE_URL_IMAGE + "w500";
        public static final String FETCH_W400_IMAGE = BASE_URL_IMAGE + "w400";
    }

    public static class Values {
        public static final String DB_NAME = "MovieDB.db";
        public static final String DB_TABLE_NAME = "NowPlaying";
        public final static String QUERY_PARAMS_API_KEY = "63d19b0e70cfc001fe8215a8f3433b09";
        public final static int QUERY_PARAMS_STARTING_PAGE = 1;
        public static final int NUMBERS_OF_THREADS = 3;
    }

    public static class Keys {
        public final static String QUERY_PARAMS_API_KEY_LABEL = "api_key";
        public final static String QUERY_PARAMS_PAGE_LABEL = "page";
        public final static String INTENT_SELECTED_MOVIE = "selected_movie";

    }
}
