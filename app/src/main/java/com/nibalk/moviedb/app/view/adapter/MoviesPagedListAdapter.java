package com.nibalk.moviedb.app.view.adapter;

import android.arch.paging.PagedListAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nibalk.moviedb.R;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.view.utils.MoviesPagedListItemClickListener;
import com.nibalk.moviedb.databinding.LayoutCardviewBinding;
import com.squareup.picasso.Picasso;

import static com.nibalk.moviedb.app.utils.Constants.AppUrls.FETCH_W500_IMAGE;

public class MoviesPagedListAdapter extends PagedListAdapter<Movie, MoviesPagedListAdapter.NowPlayingViewHolder> {

    private MoviesPagedListItemClickListener listener;
    private String selectedMovieId;

    public MoviesPagedListAdapter(MoviesPagedListItemClickListener listener, String selectedMovieId) {
        super(Movie.DIFF_CALLBACK);

        this.listener = listener;
        this.selectedMovieId = selectedMovieId;
    }

    @NonNull
    @Override
    public NowPlayingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutCardviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.layout_cardview, parent, false);

        return new NowPlayingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final NowPlayingViewHolder holder, int position) {
        Movie movie = getItem(position);

        String releaseDate = movie.getReleaseDate();
        if (!TextUtils.isEmpty(releaseDate)) {
            movie.setReleaseYear(releaseDate.substring(0, Math.min(releaseDate.length(), 4)));
        }

        double voteAverage = movie.getVoteAverage();
        movie.setRating((float) voteAverage/2);

        holder.binding.setModel(movie);

        if(movie.getPosterPath() != null) {
            String poster = FETCH_W500_IMAGE + movie.getPosterPath();
            Picasso.get()
                    .load(poster)
                    .into(holder.binding.ivPoster);
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public Movie getAdapterItem(int position) {
        return getItem(position);
    }

    class NowPlayingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        LayoutCardviewBinding binding;

        NowPlayingViewHolder(LayoutCardviewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

            boolean isNowPlayingList = TextUtils.isEmpty(selectedMovieId);

            ViewGroup.LayoutParams layoutParams = binding.movieCardView.getLayoutParams();
            if (isNowPlayingList) { //now playing movie list
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else { //similar movies list
                layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            }
            binding.movieCardView.setLayoutParams(layoutParams);

            binding.movieCardView.setOnClickListener(this);

            binding.rlLanguageLogo.setVisibility(isNowPlayingList ? View.VISIBLE : View.GONE);
            binding.tvReleaseYear.setVisibility(isNowPlayingList ? View.VISIBLE : View.GONE);

        }

        @Override
        public void onClick(View v) {
            listener.onClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            listener.onLongClicked(getAdapterPosition());
            return true;
        }
    }
}