package com.nibalk.moviedb.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MoviesList {

    @Expose
    @SerializedName("page")
    private int page;
    @Expose
    @SerializedName("total_pages")
    private int totalPages;
    @Expose
    @SerializedName("dates")
    private Dates dates;
    @Expose
    @SerializedName("total_results")
    private int totalResults;
    @Expose
    @SerializedName("results")
    private ArrayList<Movie> result;


    public MoviesList(int page, int totalPages, Dates dates, int totalResults, ArrayList<Movie> result) {
        this.page = page;
        this.totalPages = totalPages;
        this.dates = dates;
        this.totalResults = totalResults;
        this.result = result;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public Dates getDates() {
        return dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public ArrayList<Movie> getResult() {
        return result;
    }

    public void setResult(ArrayList<Movie> result) {
        this.result = result;
    }

    public static class Dates {
        @Expose
        @SerializedName("minimum")
        private String Minimum;
        @Expose
        @SerializedName("maximum")
        private String Maximum;

        public String getMinimum() {
            return Minimum;
        }

        public void setMinimum(String Minimum) {
            this.Minimum = Minimum;
        }

        public String getMaximum() {
            return Maximum;
        }

        public void setMaximum(String Maximum) {
            this.Maximum = Maximum;
        }
    }
}
