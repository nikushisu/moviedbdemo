package com.nibalk.moviedb.app.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static com.nibalk.moviedb.app.utils.Constants.Values.DB_TABLE_NAME;

@Entity(tableName = DB_TABLE_NAME)
public class Movie implements Parcelable {

    @Expose
    @SerializedName("id")
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "id")
    private Integer id;

    @Expose
    @SerializedName("release_date")
    @ColumnInfo(name = "releaseDate")
    private String releaseDate;

    @Expose
    @SerializedName("overview")
    @ColumnInfo(name = "overview")
    private String overview;

    @Expose
    @SerializedName("adult")
    @ColumnInfo(name = "adult")
    private boolean adult;

    @Expose
    @SerializedName("backdrop_path")
    @ColumnInfo(name = "backdropPath")
    private String backdropPath;

    @Expose
    @SerializedName("original_title")
    @ColumnInfo(name = "originalTitle")
    private String originalTitle;

    @Expose
    @SerializedName("original_language")
    @ColumnInfo(name = "originalLanguage")
    private String originalLanguage;

    @Expose
    @SerializedName("poster_path")
    @ColumnInfo(name = "posterPath")
    private String posterPath;

    @Expose
    @SerializedName("popularity")
    @ColumnInfo(name = "popularity")
    private double popularity;

    @Expose
    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;

    @Expose
    @SerializedName("vote_average")
    @ColumnInfo(name = "voteAverage")
    private double voteAverage;

    @Expose
    @SerializedName("video")
    @ColumnInfo(name = "video")
    private boolean video;

    @Expose
    @SerializedName("vote_count")
    @ColumnInfo(name = "voteCount")
    private int voteCount;

    @Expose
    @ColumnInfo(name = "releaseYear")
    private String releaseYear;

    @Expose
    @ColumnInfo(name = "rating")
    private float rating;

    public Movie(@NonNull Integer id, String releaseDate, String overview, boolean adult,
                 String backdropPath, String originalTitle, String originalLanguage,
                 String posterPath, double popularity, String title,
                 double voteAverage, boolean video, int voteCount) {
        this.id = id;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.adult = adult;
        this.backdropPath = backdropPath;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.posterPath = posterPath;
        this.popularity = popularity;
        this.title = title;
        this.voteAverage = voteAverage;
        this.video = video;
        this.voteCount = voteCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", releaseDate='" + releaseDate + '\'' +
                ", overview='" + overview + '\'' +
                ", adult=" + adult +
                ", backdropPath='" + backdropPath + '\'' +
                ", originalTitle='" + originalTitle + '\'' +
                ", originalLanguage='" + originalLanguage + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", popularity=" + popularity +
                ", title='" + title + '\'' +
                ", voteAverage=" + voteAverage +
                ", video=" + video +
                ", voteCount=" + voteCount +
                ", releaseYear=" + releaseYear +
                ", rating=" + rating +
                '}';
    }

    public static DiffUtil.ItemCallback<Movie> DIFF_CALLBACK = new DiffUtil.ItemCallback<Movie>() {
        @Override
        public boolean areItemsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.releaseDate);
        dest.writeString(this.overview);
        dest.writeByte(this.adult ? (byte) 1 : (byte) 0);
        dest.writeString(this.backdropPath);
        dest.writeString(this.originalTitle);
        dest.writeString(this.originalLanguage);
        dest.writeString(this.posterPath);
        dest.writeDouble(this.popularity);
        dest.writeString(this.title);
        dest.writeDouble(this.voteAverage);
        dest.writeByte(this.video ? (byte) 1 : (byte) 0);
        dest.writeInt(this.voteCount);
        dest.writeString(this.releaseYear);
        dest.writeFloat(this.rating);
    }

    protected Movie(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.releaseDate = in.readString();
        this.overview = in.readString();
        this.adult = in.readByte() != 0;
        this.backdropPath = in.readString();
        this.originalTitle = in.readString();
        this.originalLanguage = in.readString();
        this.posterPath = in.readString();
        this.popularity = in.readDouble();
        this.title = in.readString();
        this.voteAverage = in.readDouble();
        this.video = in.readByte() != 0;
        this.voteCount = in.readInt();
        this.releaseYear = in.readString();
        this.rating = in.readFloat();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
