package com.nibalk.moviedb.app.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nibalk.moviedb.app.data.model.Movie;

import static com.nibalk.moviedb.app.utils.Constants.Values.DB_NAME;

@Database(entities = {Movie.class}, version = 1, exportSchema = false)
public abstract class MoviesDB extends RoomDatabase {

    private static MoviesDB instance;

    public abstract MoviesDao getMoviesDao();

    public static MoviesDB getMoviesDB(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    MoviesDB.class, DB_NAME)
                    .build();
        }
        return instance;
    }
}
