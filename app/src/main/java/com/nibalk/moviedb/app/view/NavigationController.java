package com.nibalk.moviedb.app.view;

import android.content.Context;
import android.content.Intent;

import com.nibalk.moviedb.R;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.view.activity.DetailActivity;

import static com.nibalk.moviedb.app.utils.Constants.Keys.INTENT_SELECTED_MOVIE;

public class NavigationController {

    public void addDetailActivity(Context context, Movie movie) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(INTENT_SELECTED_MOVIE, movie);
        context.startActivity(intent);
    }
}
