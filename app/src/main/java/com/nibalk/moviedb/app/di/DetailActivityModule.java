package com.nibalk.moviedb.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.di.annotation.ActivityScope;
import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.moviedb.app.data.repository.MovieRepository;
import com.nibalk.moviedb.app.view.NavigationController;
import com.nibalk.moviedb.app.viewmodel.DetailActivityViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailActivityModule {

    @Provides
    DetailActivityViewModel detailActivityViewModel(MovieRepository repository) {
        return new DetailActivityViewModel(repository);
    }

    @Provides
    ViewModelProvider.Factory provideDetailActivityViewModel(DetailActivityViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }

    @Provides
    @ActivityScope
    public NavigationController provideNavigationController() {
        return new NavigationController();
    }
}
