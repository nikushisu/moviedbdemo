package com.nibalk.moviedb.app.data.datasource.local;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.nibalk.moviedb.app.data.database.MoviesDao;
import com.nibalk.moviedb.app.data.model.Movie;

import java.util.List;

import timber.log.Timber;

public class MovieLocalPageKeyedDataSource extends PageKeyedDataSource<Integer, Movie> {

    private MoviesDao moviesDao;

    public MovieLocalPageKeyedDataSource(MoviesDao moviesDao) {
        this.moviesDao = moviesDao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Movie> callback) {
        Timber.d("Loading Initial Rang, Count " + params.requestedLoadSize);
        List<Movie> list = moviesDao.selectAllMovies();
        if(list.size() > 0) {
            callback.onResult(list, 0, 1);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {

    }

}
