package com.nibalk.moviedb.app.view.utils;

public interface MoviesPagedListItemClickListener {

    void onClicked(int position);

    void onLongClicked(int position);
}
