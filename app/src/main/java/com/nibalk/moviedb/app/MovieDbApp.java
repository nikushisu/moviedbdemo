package com.nibalk.moviedb.app;

import android.app.Activity;
import android.app.Application;

import com.nibalk.moviedb.app.di.AppComponent;
import com.nibalk.moviedb.app.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

public class MovieDbApp extends Application implements HasActivityInjector {

    private AppComponent appComponent;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
        initTimberLogs();
    }

    private void initDagger() {
        AppComponent component = DaggerAppComponent.builder()
                .application(this)
                .build();
        component.inject(this);
        setAppComponent(component);
    }

    private void initTimberLogs() {
        Timber.plant(new Timber.DebugTree());
        Timber.tag("DUApp");
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public void setAppComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }
}
