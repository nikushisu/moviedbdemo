package com.nibalk.moviedb.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.di.annotation.ActivityScope;
import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.moviedb.app.data.repository.MovieRepository;
import com.nibalk.moviedb.app.view.NavigationController;
import com.nibalk.moviedb.app.viewmodel.MainActivityViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    MainActivityViewModel mainActivityViewModel(MovieRepository movieRepository) {
        return new MainActivityViewModel(movieRepository);
    }

    @Provides
    ViewModelProvider.Factory provideMainActivityViewModel(MainActivityViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }

    @Provides
    @ActivityScope
    public NavigationController provideNavigationController() {
        return new NavigationController();
    }

}
