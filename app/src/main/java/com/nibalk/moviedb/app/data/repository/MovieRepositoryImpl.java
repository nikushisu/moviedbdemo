package com.nibalk.moviedb.app.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.paging.PagedList;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.nibalk.moviedb.app.data.database.MoviesDB;
import com.nibalk.moviedb.app.data.datasource.local.MovieLocalDataSourceFactory;
import com.nibalk.moviedb.app.data.datasource.remote.MovieRemoteDataSourceFactory;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.repository.local.MovieLocalRepository;
import com.nibalk.moviedb.app.data.repository.remote.MovieRemoteRepository;
import com.nibalk.moviedb.app.data.service.MovieDbService;
import com.nibalk.framework.network.service.WebServiceManager;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class MovieRepositoryImpl implements MovieRepository {
    
    private MoviesDB database;
    private MovieRemoteRepository remoteRepository;
    private MovieLocalRepository localRepository;
    private MovieRemoteDataSourceFactory remoteDataSourceFactory;
    private MovieLocalDataSourceFactory localDataSourceFactory;

    private CompositeDisposable compositeDisposable;
    private MediatorLiveData liveDataMerger;

    private MovieDbService movieDbService;
    private WebServiceManager webServiceManager;

    private boolean isFetchingNowPlaying = false;


    public MovieRepositoryImpl(Context context, MovieDbService movieDbService,
                               WebServiceManager webServiceManager) {

        this.database = MoviesDB.getMoviesDB(context.getApplicationContext());

        this.movieDbService = movieDbService;
        this.webServiceManager = webServiceManager;
    }

    @Override
    public void initFetching(CompositeDisposable disposable, String selectedMovieId) {

        Timber.d("initFetching()");

        isFetchingNowPlaying = TextUtils.isEmpty(selectedMovieId);

        compositeDisposable = disposable;
        remoteDataSourceFactory = new MovieRemoteDataSourceFactory(movieDbService,
                webServiceManager, compositeDisposable, selectedMovieId);
        localDataSourceFactory = new MovieLocalDataSourceFactory(
                database.getMoviesDao());

        remoteRepository = new MovieRemoteRepository(remoteDataSourceFactory, boundaryCallback);
        localRepository = new MovieLocalRepository(localDataSourceFactory);
        liveDataMerger = new MediatorLiveData<>();

        liveDataMerger.addSource(remoteRepository.getMoviesPagedList(), new Observer() {
            @Override
            public void onChanged(@Nullable Object value) {
                Timber.d("AddSource Remote - " + value.toString());
                liveDataMerger.setValue(value);
            }
        });

        getMovies(selectedMovieId);
    }

    @Override
    public LiveData<PagedList<Movie>> getMovies(){
        return  liveDataMerger;
    }

    @Override
    public LiveData<ProgressStatus> getProgressStatus() {
        return remoteRepository.getLiveProgressStatus();
    }

    private void getMovies(String selectedMovieId) {
        Timber.d("selectAllMovies()");

        Disposable disposable = remoteDataSourceFactory.getMovies()
                .observeOn(Schedulers.io())
                .subscribe(movie -> {
                    Timber.d("selectAllMovies - response");

                    if (movie != null) {
                        Timber.d("selectAllMovies - response - success");

                        if (TextUtils.isEmpty(selectedMovieId)) { //when fetching now playing list, store to local db
                            Timber.d("Storing Data == " + movie.toString());
                            database.getMoviesDao().insertMovies(movie);
                        }
                    } else {
                        Timber.d("selectAllMovies - response - fail");
                    }
                }, throwable -> {
                    Timber.d("selectAllMovies - response - error");
                });
        compositeDisposable.add(disposable);
    }

    private PagedList.BoundaryCallback<Movie> boundaryCallback = new PagedList.BoundaryCallback<Movie>() {
        @Override
        public void onZeroItemsLoaded() {
            super.onZeroItemsLoaded();
            liveDataMerger.addSource(localRepository.getMoviesPagedList(), value -> {
                Timber.d("AddSource Local -- " + value.toString());
                liveDataMerger.setValue(value);
                liveDataMerger.removeSource(localRepository.getMoviesPagedList());
            });
        }
    };
}
