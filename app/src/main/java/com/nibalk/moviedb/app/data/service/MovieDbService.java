package com.nibalk.moviedb.app.data.service;

import com.nibalk.moviedb.app.data.model.MoviesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.nibalk.moviedb.app.utils.Constants.AppUrls.FETCH_NOW_PLAYING;
import static com.nibalk.moviedb.app.utils.Constants.AppUrls.FETCH_SIMILAR_MOVIES;
import static com.nibalk.moviedb.app.utils.Constants.Keys.QUERY_PARAMS_API_KEY_LABEL;
import static com.nibalk.moviedb.app.utils.Constants.Keys.QUERY_PARAMS_PAGE_LABEL;

public interface MovieDbService {

    @GET(FETCH_NOW_PLAYING)
    Call<MoviesList> fetchMoviesList(@Query(QUERY_PARAMS_API_KEY_LABEL) String apiKey,
                                     @Query(QUERY_PARAMS_PAGE_LABEL) int page);

    @GET(FETCH_SIMILAR_MOVIES)
    Call<MoviesList> fetchSimilarMoviesList(@Path(value = "movie_id") String userId,
                                            @Query(QUERY_PARAMS_API_KEY_LABEL) String apiKey);

}
