package com.nibalk.moviedb.app.data.repository.remote;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.nibalk.moviedb.app.data.datasource.remote.MovieRemoteDataSourceFactory;
import com.nibalk.moviedb.app.data.datasource.remote.MovieRemotePageKeyedDataSource;
import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.model.Movie;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.nibalk.moviedb.app.utils.Constants.Values.NUMBERS_OF_THREADS;

public class MovieRemoteRepository {

    final private static String TAG = "DUApp-" + MovieRemoteRepository.class.getSimpleName();

    private LiveData<PagedList<Movie>> moviesPagedList;
    private LiveData<ProgressStatus> liveProgressStatus;

    public MovieRemoteRepository(MovieRemoteDataSourceFactory dataSourceFactory,
                                 PagedList.BoundaryCallback<Movie> boundaryCallback){

        PagedList.Config pagedListConfig =
                new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(10)
                        .setPageSize(10).build();

        liveProgressStatus = Transformations.switchMap(dataSourceFactory.getRemoteDataSourceLive(),
                (Function<MovieRemotePageKeyedDataSource, LiveData<ProgressStatus>>)
                MovieRemotePageKeyedDataSource::getProgressLiveStatus);

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder<>(
                dataSourceFactory, pagedListConfig);
        moviesPagedList = livePagedListBuilder.
                setFetchExecutor(executor).
                setBoundaryCallback(boundaryCallback).
                build();

    }

    public LiveData<PagedList<Movie>> getMoviesPagedList() {
        return moviesPagedList;
    }

    public LiveData<ProgressStatus> getLiveProgressStatus() {
        return liveProgressStatus;
    }
}
