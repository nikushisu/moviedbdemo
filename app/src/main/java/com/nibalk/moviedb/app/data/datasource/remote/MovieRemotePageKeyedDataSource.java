package com.nibalk.moviedb.app.data.datasource.remote;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.nibalk.framework.network.service.WebServiceManager;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.model.MoviesList;
import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.service.MovieDbService;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import retrofit2.Call;
import timber.log.Timber;

import static com.nibalk.moviedb.app.utils.Constants.Values.QUERY_PARAMS_API_KEY;
import static com.nibalk.moviedb.app.utils.Constants.Values.QUERY_PARAMS_STARTING_PAGE;

public class MovieRemotePageKeyedDataSource extends PageKeyedDataSource<Integer, Movie> {

    private MovieDbService movieDbService;
    private WebServiceManager webServiceManager;
    private CompositeDisposable compositeDisposable;
    private String selectedMovieId;

    private MutableLiveData progressLiveStatus;
    private ReplaySubject<Movie> movies;

    private ArrayList<Movie> emptyList = new ArrayList<>();

    public MovieRemotePageKeyedDataSource(MovieDbService movieDbService,
                                          WebServiceManager webServiceManager,
                                          CompositeDisposable compositeDisposable,
                                          String selectedMovieId) {
        this.movieDbService = movieDbService;
        this.webServiceManager = webServiceManager;
        this.compositeDisposable = compositeDisposable;
        this.selectedMovieId = selectedMovieId;

        this.progressLiveStatus = new MutableLiveData<>();
        this.movies = ReplaySubject.create();

        setMovies(new ArrayList<>());
    }

    public MutableLiveData getProgressLiveStatus() {
        return progressLiveStatus;
    }

    public ReplaySubject<Movie> getMovies() {
        return movies;
    }

    private Observable<MoviesList> fetchMovies(int page) {
        Timber.d("fetchMovies()");

        Call<MoviesList> call;
        if (TextUtils.isEmpty(selectedMovieId)) { //fetching now playing list
            Timber.d("fetchMovies() - NowPlaying");
            call = movieDbService.fetchMoviesList(QUERY_PARAMS_API_KEY, page);
        } else { //fetching similar movies
            Timber.d("fetchMovies() - Similar - " + selectedMovieId);
            call = movieDbService.fetchSimilarMoviesList(selectedMovieId, QUERY_PARAMS_API_KEY);
        }

        return webServiceManager.execute(call)
                .map(moviesList -> {
                    if (moviesList != null) {
                        Timber.d("Fetched MoviesList count = " + moviesList.getResult().size());
                        Timber.d(moviesList.toString());
                    } else {
                        Timber.d("Fetched MoviesList content is null");
                    }
                    return moviesList;
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params,
                            @NonNull LoadInitialCallback<Integer, Movie> callback) {

        Timber.d("loadInitial");

        Disposable disposable = fetchMovies(QUERY_PARAMS_STARTING_PAGE)
                .doOnSubscribe(disposable1 -> progressLiveStatus.postValue(ProgressStatus.RUNNING))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moviesList -> {
                    Timber.d("loadInitial - response");

                    if (moviesList != null) {
                        Timber.d("loadInitial - response - success");
                        progressLiveStatus.setValue(ProgressStatus.SUCCESS);

                        ArrayList<Movie> list = moviesList.getResult();
                        callback.onResult(list, QUERY_PARAMS_STARTING_PAGE,
                                QUERY_PARAMS_STARTING_PAGE + 1);
                        setMovies(list);

                    } else {
                        Timber.d("loadInitial - response - fail");
                        progressLiveStatus.setValue(ProgressStatus.FAILED);

                        callback.onResult(emptyList, QUERY_PARAMS_STARTING_PAGE,
                                QUERY_PARAMS_STARTING_PAGE + 1);
                        setMovies(emptyList);
                    }
                }, throwable -> {
                    Timber.d("loadInitial - error");
                    progressLiveStatus.setValue(ProgressStatus.ERROR);

                    callback.onResult(emptyList, QUERY_PARAMS_STARTING_PAGE,
                            QUERY_PARAMS_STARTING_PAGE + 1);
                    setMovies(emptyList);
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params,
                           @NonNull LoadCallback<Integer, Movie> callback) {

    }

    @SuppressLint("CheckResult")
    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params,
                          @NonNull LoadCallback<Integer, Movie> callback) {

        Timber.d("loadAfter");

        Disposable disposable = fetchMovies(params.key)
                .doOnSubscribe(disposable1 -> progressLiveStatus.postValue(ProgressStatus.RUNNING))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moviesList -> {
                    Timber.d("loadAfter - response");

                    if (moviesList != null) {
                        Timber.d("loadAfter - response - success");
                        progressLiveStatus.setValue(ProgressStatus.SUCCESS);

                        ArrayList<Movie> list = moviesList.getResult();
                        callback.onResult(list,params.key + 1);
                        setMovies(list);

                    } else {
                        Timber.d("loadAfter - response - fail");
                        progressLiveStatus.setValue(ProgressStatus.FAILED);

                        callback.onResult(emptyList,params.key + 1);
                        setMovies(emptyList);
                    }
                }, throwable -> {
                    Timber.d("loadAfter - response - error");
                    progressLiveStatus.setValue(ProgressStatus.ERROR);

                    callback.onResult(emptyList,params.key + 1);
                    setMovies(emptyList);
                });
        compositeDisposable.add(disposable);
    }

    private void setMovies(ArrayList<Movie> list) {
        ReplaySubject<Movie> moviesReplaySubject = movies;
        for (Movie movie : list) {
            moviesReplaySubject.onNext(movie);
        }
        Timber.d("setMovies - " + moviesReplaySubject.toString());
    }
}
