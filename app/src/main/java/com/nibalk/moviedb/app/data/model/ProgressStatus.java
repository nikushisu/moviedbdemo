package com.nibalk.moviedb.app.data.model;

public enum ProgressStatus {

    RUNNING(""),
    SUCCESS(""),
    FAILED(""),
    ERROR("");

    private final String message;


    ProgressStatus(String message) {
        this.message = message;
    }


}
