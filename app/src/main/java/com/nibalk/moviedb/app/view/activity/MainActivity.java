package com.nibalk.moviedb.app.view.activity;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.nibalk.framework.base.view.BaseActivity;
import com.nibalk.moviedb.R;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.view.NavigationController;
import com.nibalk.moviedb.app.view.adapter.MoviesPagedListAdapter;
import com.nibalk.moviedb.app.view.utils.MoviesPagedListItemClickListener;
import com.nibalk.moviedb.app.viewmodel.MainActivityViewModel;
import com.nibalk.moviedb.databinding.ActivityMainBinding;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity<MainActivityViewModel, ActivityMainBinding> {

    @Inject
    ConnectivityManager connectivityManager;

    @Inject
    NavigationController navigationController;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    private MoviesPagedListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d("onCreate");
        setContentView(R.layout.activity_main);

        super.onCreate(savedInstanceState);

        /**
         * When device is online - app will fetch data from server and store it in the SQLite DB
         * When device is offline - app will fetch data from SQLite DB
         */
        displayNowPlayingMovies();
    }

    @Override
    protected MainActivityViewModel setupViewModel() {
        Timber.d("setupViewModel");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected ActivityMainBinding setupDataBinding() {
        Timber.d("setupDataBinding");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        return binding;
    }

    private void displayNowPlayingMovies() {

        binding.list.setLayoutManager(new GridLayoutManager(this, 2));

        adapter = new MoviesPagedListAdapter(listItemClickListener, null);
        binding.list.setAdapter(adapter);

        viewModel.getNowPlayingMovies().observe(this, moviesList -> {
            adapter.submitList(moviesList);
        });

        viewModel.getProgressStatus().observe(this, status -> {
            switch (status) {
                case RUNNING:
                    binding.progress.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    binding.progress.setVisibility(View.GONE);
                    break;
                case ERROR:
                    binding.progress.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            "Device is offline. Can not connect to server.", Snackbar.LENGTH_SHORT)
                            .show();
                    break;
                case FAILED:
                    binding.progress.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            "Server connection failed", Snackbar.LENGTH_SHORT)
                            .show();
                    break;
            }
        });

    }

    private MoviesPagedListItemClickListener listItemClickListener = new MoviesPagedListItemClickListener() {
        @Override
        public void onClicked(int position) {
            Movie movie = adapter.getAdapterItem(position);

            navigationController.addDetailActivity(MainActivity.this, movie);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

        @Override
        public void onLongClicked(int position) {
            Toast.makeText(MainActivity.this, "Long clicked image at " + position,
                    Toast.LENGTH_SHORT).show();
        }
    };
}
