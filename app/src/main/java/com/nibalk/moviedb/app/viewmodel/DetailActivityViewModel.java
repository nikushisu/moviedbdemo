package com.nibalk.moviedb.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PagedList;

import com.nibalk.framework.base.viewmodel.BaseViewModel;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.repository.MovieRepository;

public class DetailActivityViewModel extends BaseViewModel {

    private final MutableLiveData movie;
    private MovieRepository repository;



    public DetailActivityViewModel(MovieRepository repository) {
        this.movie = new MutableLiveData<Movie>();
        this.repository = repository;
    }

    public MutableLiveData<Movie> getMovie() {
        return movie;
    }

    public LiveData<PagedList<Movie>> getSimilarMovies(String selectedMovieId) {
        repository.initFetching(getCompositeDisposable(), selectedMovieId);
        return repository.getMovies();
    }

    public LiveData<ProgressStatus> getProgressStatus() {
        return repository.getProgressStatus();
    }
}
