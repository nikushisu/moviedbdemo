package com.nibalk.moviedb.app.view.activity;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nibalk.framework.base.view.BaseActivity;
import com.nibalk.framework.network.utils.NetworkUtils;
import com.nibalk.moviedb.R;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.view.NavigationController;
import com.nibalk.moviedb.app.view.adapter.MoviesPagedListAdapter;
import com.nibalk.moviedb.app.view.utils.MoviesPagedListItemClickListener;
import com.nibalk.moviedb.app.viewmodel.DetailActivityViewModel;
import com.nibalk.moviedb.databinding.ActivityDetailBinding;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import timber.log.Timber;

import static com.nibalk.moviedb.app.utils.Constants.AppUrls.FETCH_W400_IMAGE;
import static com.nibalk.moviedb.app.utils.Constants.AppUrls.FETCH_W500_IMAGE;
import static com.nibalk.moviedb.app.utils.Constants.Keys.INTENT_SELECTED_MOVIE;

public class DetailActivity extends BaseActivity<DetailActivityViewModel, ActivityDetailBinding> {

    @Inject
    NavigationController navigationController;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    ConnectivityManager connectivityManager;

    private ActivityDetailBinding binding;
    private DetailActivityViewModel viewModel;

    private RecyclerView relatedMoviesView;
    private MoviesPagedListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d("onCreate");
        setContentView(R.layout.activity_detail);

        super.onCreate(savedInstanceState);

        displayMovieDetail();
    }

    @Override
    protected DetailActivityViewModel setupViewModel() {
        Timber.d("setupViewModel");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected ActivityDetailBinding setupDataBinding() {
        Timber.d("setupDataBinding");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        return binding;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @BindingAdapter("android:src")
    public static void setImageUrl(ImageView view, String imagePath) {
        String base = FETCH_W500_IMAGE;
        if(imagePath != null) {
            if (view.getId() == R.id.backdrop) {
                base = FETCH_W400_IMAGE;
            }
            Picasso.get().load(base + imagePath).into(view);
        }
    }

    private void displayMovieDetail() {
        Movie movie = getIntent().getExtras().getParcelable(INTENT_SELECTED_MOVIE);
        binding.setMovie(movie);

        displayImages(movie);

        if (NetworkUtils.isInternetAvailable(connectivityManager)) {
            displayRelatedMovies(movie.getId().toString());
        } else {
            binding.similarMovieTitle.setVisibility(View.GONE);
            binding.relatedMoviesDivider.setVisibility(View.GONE);
        }
    }

    private void displayRelatedMovies(String movieId) {
        relatedMoviesView = binding.list;
        relatedMoviesView.setHasFixedSize(true);
        relatedMoviesView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));

        adapter = new MoviesPagedListAdapter(listItemClickListener, movieId);
        binding.list.setAdapter(adapter);

        viewModel.getSimilarMovies(movieId).observe(this, moviesList -> {
            adapter.submitList(moviesList);
        });
    }

    private void displayImages(Movie movie) {
        if(movie.getBackdropPath() != null) {
            Picasso.get()
                    .load(FETCH_W400_IMAGE + movie.getBackdropPath())
                    .into(binding.backdrop);
        }
        if(movie.getPosterPath() != null) {
            Picasso.get()
                    .load(FETCH_W500_IMAGE + movie.getPosterPath())
                    .into(binding.poster);
        }
    }

    private MoviesPagedListItemClickListener listItemClickListener = new MoviesPagedListItemClickListener() {
        @Override
        public void onClicked(int position) {
            Movie movie = adapter.getAdapterItem(position);
            navigationController.addDetailActivity(DetailActivity.this, movie);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

        @Override
        public void onLongClicked(int position) {
            Toast.makeText(DetailActivity.this, "Long clicked image at " + position,
                    Toast.LENGTH_SHORT).show();
        }
    };
}
