package com.nibalk.moviedb.app.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import com.nibalk.moviedb.app.data.model.ProgressStatus;
import com.nibalk.moviedb.app.data.model.Movie;

import io.reactivex.disposables.CompositeDisposable;

public interface MovieRepository {

    void initFetching(CompositeDisposable compositeDisposable, String selectedMovieId);

    LiveData<PagedList<Movie>> getMovies();

    LiveData<ProgressStatus> getProgressStatus();

}
