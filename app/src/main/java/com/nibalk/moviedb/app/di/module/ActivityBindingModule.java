package com.nibalk.moviedb.app.di.module;

import com.nibalk.moviedb.app.di.DetailActivityModule;
import com.nibalk.moviedb.app.di.MainActivityModule;
import com.nibalk.moviedb.app.view.activity.DetailActivity;
import com.nibalk.moviedb.app.view.activity.MainActivity;
import com.nibalk.framework.base.di.annotation.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindingMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {DetailActivityModule.class})
    abstract DetailActivity bindingDetailActivity();
}
