package com.nibalk.moviedb.app.di;

import android.app.Application;

import com.nibalk.moviedb.app.MovieDbApp;
import com.nibalk.moviedb.app.di.module.ActivityBindingModule;
import com.nibalk.moviedb.app.di.module.AppModule;
import com.nibalk.framework.base.di.FrameworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, FrameworkModule.class, AppModule.class, ActivityBindingModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application app);

        AppComponent build();
    }

    void inject(MovieDbApp app);
}
