package com.nibalk.moviedb.app.data.datasource.local;

import android.arch.paging.DataSource;

import com.nibalk.moviedb.app.data.database.MoviesDao;
import com.nibalk.moviedb.app.data.model.Movie;
import com.nibalk.moviedb.app.data.repository.local.MovieLocalRepository;


public class MovieLocalDataSourceFactory extends DataSource.Factory<Integer, Movie> {

    final private static String TAG = "DUApp-" + MovieLocalRepository.class.getSimpleName();

    private MovieLocalPageKeyedDataSource localDataSource;


    public MovieLocalDataSourceFactory(MoviesDao dao) {
        localDataSource = new MovieLocalPageKeyedDataSource(dao);
    }

    @Override
    public DataSource<Integer, Movie> create() {
        return localDataSource;
    }
}
