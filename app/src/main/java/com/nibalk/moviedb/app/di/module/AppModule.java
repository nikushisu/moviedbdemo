package com.nibalk.moviedb.app.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


import static com.nibalk.moviedb.app.utils.Constants.AppUrls.BASE_URL;
import static com.nibalk.framework.base.Constants.Keys.BASE_URL_KEY;

@Module(includes = {MovieDbModule.class})
public class AppModule {

    @Provides
    @Singleton
    public Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Named(BASE_URL_KEY)
    public String provideBaseUrl() {
        return new String(BASE_URL);
    }

}
